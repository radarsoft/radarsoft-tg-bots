export type DeleteToScrapeOutput = {
  delete_fa_to_scrape: {
    affected_rows: number;
  };
};
