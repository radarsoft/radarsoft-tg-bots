export type InsertEntryInput = {
  profile: string;
  profileName: string;
  full: string;
  postId: number;
  link: string;
  title: string;
  postKey: string;
  apiVersion: string;
  thumbnail: string;
  origin: string;
};
