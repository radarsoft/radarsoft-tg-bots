import { Bot, webhookCallback } from "grammy";

const API_SERVER = Deno.env.get("TG_BOTAPI");
const BOT_TOKEN = Deno.env.get("VIX_BOT_TOKEN");
const USER_ID = Deno.env.get("VIX_USER_ID");

if (!API_SERVER || !BOT_TOKEN || !USER_ID) {
  console.error("env-vars not set, exiting with an error.");
  Deno.exit(1);
}

const VXSFNCRNR = new Bot(BOT_TOKEN, {
  client: {
    apiRoot: API_SERVER,
  },
});

VXSFNCRNR.on("message", async (ctx) => {
  await ctx.api.forwardMessage(
    USER_ID,
    ctx.message.from.id,
    ctx.message.message_id,
  );
  console.log(ctx.message);
});

const handleUpdate = webhookCallback(VXSFNCRNR, "std/http");
Deno.serve({
  port: 8101,
  onListen: async (params) => {
    console.log(`Server listening on ${params.hostname}:${params.port}`);
    await VXSFNCRNR.api.deleteWebhook();
    await VXSFNCRNR.api.setWebhook(
      `http://tg-bot-vxsfncrnr.tg-bot.svc.cluster.local/${VXSFNCRNR.token}`,
      {
        drop_pending_updates: true,
      },
    );
  },
  onError: (error) => {
    console.error(error);
    return new Response(null, { status: 500 });
  },
}, async (req, _info) => {
  try {
    const url = new URL(req.url);
    if (url.pathname.startsWith("/healthz")) {
      return new Response(null, { status: 200 });
    }
  } catch (err) {
    console.error(err);
    return new Response(err, { status: 500 });
  }

  if (req.method !== "POST") {
    return new Response("not allowed", { status: 405 });
  }

  try {
    const url = new URL(req.url);
    if (!url.pathname.startsWith(`/${VXSFNCRNR.token}`)) {
      return new Response("not allowed", { status: 405 });
    }
    return await handleUpdate(req);
  } catch (err) {
    console.error(err);
    return new Response(err, { status: 500 });
  }
});

Deno.addSignalListener("SIGTERM", () => {
  console.log("Shutting down gracefully.");
  Deno.exit(0);
});
