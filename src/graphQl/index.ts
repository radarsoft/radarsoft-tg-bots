import {
  DeleteToScrapeOutput,
  GraphQlResponse,
  InsertEntryInput,
  InsertEntryOutput,
  InsertFaToScrapeInput,
  InsertFaToScrapeOutput,
} from "graphQl/types";

export class GraphQl {
  private _url: string;
  private _defaultConfig: Partial<RequestInit>;

  private _operations = `
        mutation InsertToScrape($objects: [fa_to_scrape_insert_input!] = {post_id: 10, origin: ""}) {
            insert_fa_to_scrape(objects: $objects, on_conflict: {constraint: fa_to_scrape_origin_id_comb_key, update_columns: []}) {
                affected_rows
            }
        }

        mutation DeleteToScrape ($idCombs: [String!]){
          delete_fa_to_scrape(where: { origin_id_comb: { _in: $idCombs } }) {
            affected_rows
          }
        }
        
        mutation InsertEntry($profile: String!, $profileName: String!, $full: String!, $postId: Int!, $link: String!, $title: String!, $postKey: String!, $apiVersion: String!, $thumbnail: String, $origin: String!) {
          insert_queue_entry_one(object: {
            artist_link: $profile,
            artist_name: $profileName,
            full_link: $full,
            origin: $origin,
            post_id: $postId,
            post_link: $link,
            post_name: $title,
            post_origin_id_comb: $postKey,
            posted: false,
            saved_on_disk: false,
            saved_with_api_ver: $apiVersion,
            tg_image_link: $thumbnail
          }) {
            id
          }
        }
    `;

  constructor(url: string, authHeaderKey?: string, authHeaderValue?: string) {
    this._url = url;

    const configHeaders = new Headers({
      "Content-Type": "application/json",
    });

    if (authHeaderKey && authHeaderValue) {
      configHeaders.append(authHeaderKey, authHeaderValue);
    }

    this._defaultConfig = {
      method: "POST",
      headers: configHeaders,
    };
  }

  private _fetchGql<T>(
    operationName: string,
    variables: Record<string, unknown>,
  ): Promise<GraphQlResponse<T>> {
    const currentRequest = {
      ...this._defaultConfig,
      body: JSON.stringify({
        query: this._operations,
        variables,
        operationName,
      }),
    };

    return fetch(this._url, currentRequest).then((result) =>
      result.json() as GraphQlResponse<T>
    );
  }

  public insertToScrape(objects: InsertFaToScrapeInput[]) {
    return this._fetchGql<InsertFaToScrapeOutput>("InsertToScrape", {
      objects,
    });
  }

  public deleteToScrape(idCombs: string[]) {
    return this._fetchGql<DeleteToScrapeOutput>("DeleteToScrape", {
      idCombs,
    });
  }

  public insertEntry(entry: InsertEntryInput) {
    return this._fetchGql<InsertEntryOutput>("InsertEntry", {
      ...entry,
    });
  }
}
