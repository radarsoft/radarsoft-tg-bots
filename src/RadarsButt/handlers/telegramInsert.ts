import { GraphQl } from "graphQl";
import {
  GraphQlResponse,
  InsertEntryInput,
  InsertEntryOutput,
} from "graphQl/types";

export const insertForwardedEntry: (
  entry: InsertEntryInput,
  graphQl: GraphQl,
) => Promise<number> = async (entry, graphQl) => {
  let gqlResponse: GraphQlResponse<InsertEntryOutput> = {};
  try {
    gqlResponse = await graphQl.insertEntry(entry);
  } catch (e) {
    console.error(e);
  }

  if (gqlResponse.errors?.length) {
    throw new Error(JSON.stringify(gqlResponse.errors));
  }

  if (gqlResponse.data) {
    return gqlResponse.data.insert_queue_entry_one.id || 0;
  }

  return 0;
};
