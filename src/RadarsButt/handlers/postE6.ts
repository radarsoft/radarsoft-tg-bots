import { GraphQl } from "graphQl";
import {
  GraphQlResponse,
  InsertFaToScrapeInput,
  InsertFaToScrapeOutput,
} from "graphQl/types";

export const postE6: (ids: number[], graphQl: GraphQl) => Promise<number> =
  async (ids, graphQl) => {
    if (ids.length > 0) {
      const objects = ids.map(
        (parameter) => ({
          post_id: parameter,
          origin: "E6",
          origin_id_comb: `E6_${parameter}`,
        } as InsertFaToScrapeInput),
      );
      let gqlResponse: GraphQlResponse<InsertFaToScrapeOutput> = {};
      try {
        gqlResponse = await graphQl.insertToScrape(objects);
      } catch (e) {
        console.error(e);
      }

      if (gqlResponse.errors?.length) {
        throw new Error(JSON.stringify(gqlResponse.errors));
      }

      if (gqlResponse.data) {
        return gqlResponse.data.insert_fa_to_scrape.affected_rows || 0;
      }
    }

    return 0;
  };
