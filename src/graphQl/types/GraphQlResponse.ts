export type GraphQlResponse<T> = {
  data?: T;
  errors?: {
    message?: string;
    extensions?: {
      path?: string;
      code?: string;
    };
  }[];
};
