import { PostOrigin } from "graphQl/types";

export type InsertFaToScrapeInput = {
  post_id: number;
  origin: PostOrigin;
  origin_id_comb: string;
};
