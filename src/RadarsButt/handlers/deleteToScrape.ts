import { GraphQl } from "graphQl";
import { DeleteToScrapeOutput, GraphQlResponse } from "graphQl/types";

export const deleteToScrape: (
  originCombs: string[],
  graphQl: GraphQl,
) => Promise<number> = async (originCombs, graphQl) => {
  let gqlResponse: GraphQlResponse<DeleteToScrapeOutput> = {};
  try {
    gqlResponse = await graphQl.deleteToScrape(originCombs);
  } catch (e) {
    console.error(e);
  }

  if (gqlResponse.errors?.length) {
    throw new Error(JSON.stringify(gqlResponse.errors));
  }

  if (gqlResponse.data) {
    return gqlResponse.data.delete_fa_to_scrape.affected_rows || 0;
  }

  return 0;
};
