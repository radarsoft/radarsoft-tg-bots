FROM docker.io/lukechannings/deno:v1.39.1
WORKDIR /radarsoft-tg-bots
ADD imports.json ./
ADD src ./src
