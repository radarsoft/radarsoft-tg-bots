import { Bot, webhookCallback } from "grammy";
import { GraphQl } from "graphQl";
import { deleteToScrape, postE6 } from "RadarsButt/handlers";
import { checkPermissions } from "utils";
import { InsertEntryInput } from "graphQl/types";
import { insertForwardedEntry } from "./handlers/telegramInsert.ts";

const API_SERVER = Deno.env.get("TG_BOTAPI");
const BOT_TOKEN = Deno.env.get("RADARS_BUTT_TOKEN");
const USER_ID = Deno.env.get("USER_ID");
const GRAPHQL_URL = Deno.env.get("GQL_URL");
const GRAPHQL_AUTH = Deno.env.get("GQL_AUTH");

if (!API_SERVER || !BOT_TOKEN || !USER_ID || !GRAPHQL_URL || !GRAPHQL_AUTH) {
  console.error("env-vars not set, exiting with an error.");
  Deno.exit(1);
}

const RadarsButt = new Bot(BOT_TOKEN, {
  client: {
    apiRoot: API_SERVER,
  },
});

const GraphQlClient = new GraphQl(
  GRAPHQL_URL,
  "X-Hasura-Admin-Secret",
  GRAPHQL_AUTH,
);

RadarsButt.command("ping", (ctx) => ctx.reply("Pong!"));

RadarsButt.command("post_e6", async (ctx) => {
  if (ctx.match) {
    const hasPermissions = await checkPermissions(ctx, USER_ID);

    if (hasPermissions) {
      const ids = ctx.match.split(",").map((idStr) => parseInt(idStr));
      try {
        const affectedRows = await postE6(ids, GraphQlClient);
        await ctx.reply(
          `Sucessfully saved ${affectedRows} entr${
            affectedRows === 1 ? "y" : "ies"
          } for scraping. 🥳`,
        );
      } catch (e) {
        await ctx.reply(`⚠️⚠️⚠️ ERROR ⚠️⚠️⚠️ \n\n${e.message}`);
      }
    }
  }
});

RadarsButt.command("delete_to_scrape", async (ctx) => {
  const hasPermissions = await checkPermissions(ctx, USER_ID);

  if (hasPermissions) {
    try {
      if (!ctx.match) {
        await ctx.reply(
          "Usage:\n\n<pre><code class='language-shell'>/delete_to_scrape</code></pre>Shows this help.\n\n<pre><code class='language-shell'>/delete_to_scrape origin [postId...] [origin [postId...]...]</code></pre>Deletes the specified posts.\n<code>origin</code> - FA | E6 | KE | SF | WS | WE\n<code>postId</code> - ID of a post on the last specified origin.",
          {
            parse_mode: "HTML",
          },
        );

        return;
      }
      const args = ctx.match.split(" ").entries();
      const postOriginCombs = [];
      let currentOrigin = "";

      for (const [, arg] of args) {
        if (
          ["E6", "FA", "SF", "WE", "WS", "KE", "TG"].includes(arg.toUpperCase())
        ) {
          currentOrigin = arg.toUpperCase();
          continue;
        }

        const id = parseInt(arg, 10);

        if (!currentOrigin || isNaN(id)) {
          await ctx.reply(
            "Wrong usage format. Send the command without arguments to check the correct usage",
          );
          break;
        }

        postOriginCombs.push(`${currentOrigin}_${id}`);
      }

      if (!postOriginCombs.length) {
        await ctx.reply(
          "No ID supplied. Read the fucking help.",
        );

        return;
      }

      const affectedRows = await deleteToScrape(postOriginCombs, GraphQlClient);

      await ctx.reply(
        `Sucessfully removed ${affectedRows} entr${
          affectedRows === 1 ? "y" : "ies"
        } for scraping. 🥳`,
      );
    } catch (e) {
      await ctx.reply(`⚠️⚠️⚠️ ERROR ⚠️⚠️⚠️ \n\n${e.message}`);
    }
  }
});

RadarsButt.on("message", async (ctx) => {
  const hasPermissions = await checkPermissions(ctx, USER_ID);

  if (hasPermissions) {
    const message = ctx.message;
    try {
      if (
        message.forward_origin?.type === "channel" && !!message.photo?.length
      ) {
        const link = message.forward_origin.chat.username
          ? `tg://resolve?domain=${message.forward_origin.chat.username}&post=${message.forward_origin.message_id}`
          : `tg://privatepost?channel=${message.forward_origin.chat.id}&post=${message.forward_origin.message_id}`;
        const entry: InsertEntryInput = {
          profile: `tg://resolve?domain=${message.forward_origin.chat.username}`,
          profileName: message.forward_origin.chat.title,
          full: message.photo.at(-1)!.file_id,
          postId: message.forward_origin.message_id,
          link,
          title: message.caption || `${message.forward_origin.message_id}`,
          postKey:
            `TG_${message.forward_origin.chat.id}_${message.forward_origin.message_id}`,
          apiVersion: "TGBOT-2.5.0",
          thumbnail: message.photo.at(-1)!.file_id,
          origin: "TG",
        };

        const insertedPostId = await insertForwardedEntry(entry, GraphQlClient);

        await ctx.reply(
          `Inserted forwarded message for posting! Row id: ${insertedPostId}`,
        );
      }
    } catch (e) {
      await ctx.reply(`⚠️⚠️⚠️ ERROR ⚠️⚠️⚠️ \n\n${e.message}`);
    }
  }
});

const handleUpdate = webhookCallback(RadarsButt, "std/http");
Deno.serve({
  port: 8100,
  onListen: async (params) => {
    console.log(`Server listening on ${params.hostname}:${params.port}`);
    await RadarsButt.api.deleteWebhook();
    await RadarsButt.api.setWebhook(
      `http://tg-bot-radarsbutt.tg-bot.svc.cluster.local/${RadarsButt.token}`,
      {
        drop_pending_updates: true,
      },
    );
  },
  onError: (error) => {
    console.error(error);
    return new Response(null, { status: 500 });
  },
}, async (req, _info) => {
  try {
    const url = new URL(req.url);
    if (url.pathname.startsWith("/healthz")) {
      return new Response(null, { status: 200 });
    }
  } catch (err) {
    console.error(err);
    return new Response(err, { status: 500 });
  }

  if (req.method !== "POST") {
    return new Response("not allowed", { status: 405 });
  }

  try {
    const url = new URL(req.url);
    if (!url.pathname.startsWith(`/${RadarsButt.token}`)) {
      return new Response("not allowed", { status: 405 });
    }
    return await handleUpdate(req);
  } catch (err) {
    console.error(err);
    return new Response(err, { status: 500 });
  }
});

Deno.addSignalListener("SIGTERM", () => {
  console.log("Shutting down gracefully.");
  Deno.exit(0);
});
