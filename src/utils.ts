import { Context } from "grammy";

export const checkPermissions: (
  ctx: Context,
  validUserId: string,
) => Promise<boolean> = async (ctx, validUserId) => {
  if (ctx.update.message && `${ctx.update.message.from?.id}` !== validUserId) {
    await ctx.reply("You are not allowed to use that.", {
      reply_to_message_id: ctx.update.message.message_id,
    });

    return false;
  }

  return true;
};
