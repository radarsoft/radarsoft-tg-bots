import { Bot, webhookCallback } from "grammy";

const API_SERVER = Deno.env.get("TG_BOTAPI");
const BOT_TOKEN = Deno.env.get("RADARS_BUTT_JR_TOKEN");
const USER_ID = Deno.env.get("USER_ID");

if (!API_SERVER || !BOT_TOKEN || !USER_ID) {
  console.error("env-vars not set, exiting with an error.");
  Deno.exit(1);
}

const RadarsButtJr = new Bot(BOT_TOKEN, {
  client: {
    apiRoot: API_SERVER,
  },
});

RadarsButtJr.command("ping", (ctx) => ctx.reply("Pong!"));

RadarsButtJr.on("message", (ctx) => {
  console.log(ctx.message);
  const reply = "```javascript\n" + JSON.stringify(ctx.message, null, 2) +
    "```";
  ctx.reply(reply, {
    parse_mode: "MarkdownV2",
  });
});

const handleUpdate = webhookCallback(RadarsButtJr, "std/http");
Deno.serve({
  port: 8102,
  onListen: async (params) => {
    console.log(`Server listening on ${params.hostname}:${params.port}`);
    await RadarsButtJr.api.deleteWebhook();
    await RadarsButtJr.api.setWebhook(
      `http://tg-bot-radarsbuttjr.tg-bot.svc.cluster.local/${RadarsButtJr.token}`,
      {
        drop_pending_updates: true,
      },
    );
  },
  onError: (error) => {
    console.error(error);
    return new Response(null, { status: 500 });
  },
}, async (req, _info) => {
  try {
    const url = new URL(req.url);
    if (url.pathname.startsWith("/healthz")) {
      return new Response(null, { status: 200 });
    }
  } catch (err) {
    console.error(err);
    return new Response(err, { status: 500 });
  }

  if (req.method !== "POST") {
    return new Response("not allowed", { status: 405 });
  }

  try {
    const url = new URL(req.url);
    if (!url.pathname.startsWith(`/${RadarsButtJr.token}`)) {
      return new Response("not allowed", { status: 405 });
    }
    return await handleUpdate(req);
  } catch (err) {
    console.error(err);
    return new Response(err, { status: 500 });
  }
});

Deno.addSignalListener("SIGTERM", () => {
  console.log("Shutting down gracefully.");
  Deno.exit(0);
});
