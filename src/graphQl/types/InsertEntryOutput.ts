export type InsertEntryOutput = {
  insert_queue_entry_one: {
    id: number;
  };
};
